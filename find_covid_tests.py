#!/usr/bin/env python
from __future__ import annotations, generator_stop

import asyncio
import enum
import json
import re
import textwrap
from argparse import ArgumentParser, FileType, Namespace
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from datetime import datetime as dt
from enum import Enum
from functools import cached_property
from http.cookiejar import CookieJar
from io import TextIOWrapper
from multiprocessing import cpu_count
from random import choice
from typing import (Any, List, Literal, NewType, Optional, TypedDict)
from urllib.parse import quote
from urllib.request import (
    BaseHandler, HTTPCookieProcessor, HTTPHandler,
    HTTPPasswordMgrWithDefaultRealm, HTTPRedirectHandler, HTTPSHandler,
    OpenerDirector, ProxyBasicAuthHandler, ProxyHandler, build_opener,
    urlcleanup, urlopen
)

import yaml
from tabulate import tabulate
from yaml import SafeLoader

DURATION = dt.now()


@enum.unique
class OutputFormatEnum(Enum):
  JSON = 0
  TXT = 1


@enum.unique
class MoodEnum(Enum):
  CALM = 0
  AGGRESSIVE = 1


#
# Proxy types
#
ProxyRealmType = NewType('ProxyRealmType', str)
ProxyCredType = NewType('ProxyCredType', str)
ProxyAuthType = TypedDict(
    'ProxyAuthType', {
        'realm': Optional[ProxyRealmType],
        'username': Optional[ProxyCredType],
        'password': Optional[ProxyCredType],
    }
)
ProxyProtocolType = Literal['http']
ProxyIPType = NewType('ProxyIPType', str)
ProxyPortType = NewType('ProxyPortType', int)
ProxyType = TypedDict(
    'ProxyType', {
        'protocol': ProxyProtocolType,
        'ip': ProxyIPType,
        'port': ProxyPortType,
    }
)
ProxyListType = List[ProxyType]
ProxyRootType = TypedDict(
    'ProxyRootType', {
        'auth': ProxyAuthType,
        'proxies': ProxyListType,
    }
)

#
# API Types
#
RadiusType = NewType('RadiusType', int)
ZipcodeType = NewType('ZipcodeType', str)
URIType = NewType('URIType', str)
VisitorIDType = NewType('VisitorIDType', str)
APIKeyType = NewType('APIKeyType', str)
LocationIDType = NewType('LocationIDType', str)
ProductIDType = NewType('ProductIDType', str)


class QueryParams(object):
  def __init__(self, radius: RadiusType, zipcode: ZipcodeType):
    self.radius = radius
    self.zipcode = zipcode

  @staticmethod
  async def _genQueryGeoAPI() -> ZipcodeType:
    with urlopen('https://ipapi.co/postal/') as res:
      return ZipcodeType(res.read().decode('utf8').strip())

  @staticmethod
  async def genFromArgs(args: Namespace) -> QueryParams:
    zipcode = args.zip or await QueryParams._genQueryGeoAPI()
    return QueryParams(
        args.radius,
        zipcode,
    )


class RandomProxyHandlerFactory(object):
  """Generate ProxyHandlers with randomly selected proxy servers."""
  def __init__(self, auth: ProxyAuthType, proxies: ProxyListType):
    self.password_mgr = HTTPPasswordMgrWithDefaultRealm()
    self.password_mgr.add_password(
        auth.get('realm'),
        '*',
        auth.get('username') or '',
        auth.get('password') or '',
    )
    self.proxy_auth = ProxyBasicAuthHandler(self.password_mgr)
    self.proxies = proxies

  def getProxyBasicAuthHandler(self) -> ProxyBasicAuthHandler:
    return self.proxy_auth

  def getRandomProxyHandler(self) -> ProxyHandler:
    random_proxy = choice(self.proxies)
    proxy_handler = ProxyHandler({
        random_proxy['protocol']: f"{random_proxy['ip']}:{random_proxy['port']}"
    })

    return proxy_handler

  @staticmethod
  async def genRandomProxyHandlerFactory(
      proxy_file: TextIOWrapper
  ) -> RandomProxyHandlerFactory:
    if proxy_file is None:
      return None
    with proxy_file:
      proxy_root: ProxyRootType = yaml.load(proxy_file, SafeLoader)['proxy']

    return RandomProxyHandlerFactory(
        proxy_root['auth'],
        proxy_root['proxies'],
    )


class Context(object):
  """Program context."""
  def __init__(
      self, output_format: OutputFormatEnum, output_file: TextIOWrapper,
      mood: MoodEnum
  ):
    self.output_format = output_format
    self.output_file = output_file
    self.mood = mood

  @staticmethod
  def getFromArgs(args: Namespace) -> Context:
    mood = MoodEnum[args.mood.upper()]

    return Context(
        OutputFormatEnum[args.format.upper()],
        args.output,
        mood,
    )


class TargetEndpoints(Enum):
  HOME = 'https://www.target.com/'  # Give me your cookies

  @staticmethod
  def createStoresURI(env: Environment) -> URIType:
    return URIType(
        f"https://api.target.com/location_fulfillment_aggregations/v1/preferred_stores?zipcode={env.query_params.zipcode}&key={env.api_key}&radius={env.query_params.radius}"
    )

  @staticmethod
  def createSearchURI(
      env: Environment, store_ids: List[LocationIDType]
  ) -> URIType:
    return URIType(
        f"https://redsky.target.com/redsky_aggregations/v1/web/plp_search_v1?key={env.api_key}&category=4yink&channel=WEB&count=24&default_purchasability_filter=true&faceted_value=e6ip2wmrx4&include_sponsored=true&offset=0&page=/c/4yink&platform=desktop&pricing_store_id={store_ids[0]}&store_ids={','.join(store_ids)}&visitor_id={env.visitor_id}"
    )

  @staticmethod
  def createAvailabilityURI(
      env: Environment, product_id: ProductIDType
  ) -> URIType:
    return URIType(
        f"https://redsky.target.com/redsky_aggregations/v1/web_platform/fiats_v1?key={env.api_key}&nearby={env.query_params.zipcode}&limit=20&requested_quantity=1&radius={env.query_params.radius}&include_only_available_stores=true&tcin={product_id}"
    )


@dataclass
class Environment(object):
  """Execution environment."""
  query_params: QueryParams
  proxy_fac: RandomProxyHandlerFactory
  context: Context
  cookies: CookieJar = CookieJar()
  visitor_id: Optional[VisitorIDType] = None

  API_KEY_RE = re.compile(r'\"apiKey\":\"(?P<api_key>[a-z0-9]+)\"')
  api_key: Optional[APIKeyType] = None

  USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'

  @cached_property
  def shouldUseProxy(self) -> bool:
    return self.proxy_fac is not None

  @cached_property
  def shouldBeCalm(self) -> bool:
    return self.context.mood == MoodEnum.CALM

  def _getHandlers(self) -> List[BaseHandler]:
    if self.shouldUseProxy:
      return [
          self.proxy_fac.getRandomProxyHandler(),
          self.proxy_fac.getProxyBasicAuthHandler(),
          HTTPCookieProcessor(self.cookies),
      ]
    else:
      return [
          HTTPHandler(),
          HTTPRedirectHandler(),
          HTTPCookieProcessor(self.cookies),
          HTTPSHandler(),
      ]

  async def _genSearchVisitorID(self) -> None:
    visitor_id = [c for c in self.cookies
                  if c.name.lower() == 'visitorid'].pop().value
    self.visitor_id = (
        VisitorIDType(visitor_id) if visitor_id is not None else None
    )

  async def _genSearchAPIKey(self, line: bytes) -> None:
    line_str = line.decode('utf8').strip()
    if len(line_str) and 'apiKey' in line_str:
      result = self.API_KEY_RE.search(line_str)
      self.api_key = (
          APIKeyType(result.groupdict()['api_key'])
          if result is not None else None
      )

  async def _genStealCookies(self, opener: OpenerDirector) -> None:
    """Visiting the site to grab things to cloak with."""
    with opener.open(TargetEndpoints.HOME.value) as res:
      api_key_tasks = [
          asyncio.create_task(self._genSearchAPIKey(l)) for l in res
      ]
    await asyncio.create_task(self._genSearchVisitorID())
    await asyncio.gather(*api_key_tasks)

  async def genOpenerForRequest(self) -> OpenerDirector:
    opener = build_opener(*self._getHandlers())
    opener.addheaders = [
        ('User-Agent', self.USER_AGENT),
        ('Referer', 'https://www.target.com/'),
    ]
    await asyncio.create_task(self._genStealCookies(opener))

    return opener


def printTable(
    end_time: Any, location_ids: Any, product_ids: Any, avails: Any
) -> None:
  location_ids = list(location_ids)
  products = {
      pid: {
          'title': p['item']['product_description']['title'],
          'price': p['price']['formatted_current_price'],
      }
      for pid, p in product_ids.items()
  }
  avails = {
      pid: {
          'location_id': meta['location_id'],
          'quantity': meta['location_available_to_promise_quantity'],
          'store_name': meta['store_name'],
          'address': meta['store_address'],
          'distance': meta['distance'],
      }
      for pid, meta in avails.items()
  }
  distances_per_item = sorted({
      pid: meta['distance']
      for pid, meta in avails.items()
  })
  best_choice_pid = None
  quantity = 0
  for pid in distances_per_item:
    check_quantity = avails[pid]['quantity']
    if check_quantity > quantity:
      quantity = check_quantity
      best_choice_pid = pid

  duration = end_time - DURATION
  print(
      f"\nSearched {len(location_ids)} locations for {len(products)} COVID test kits in {duration.seconds}s {duration.microseconds / 1000}ms"
  )

  print(
      textwrap.fill(
          '\nThis is the closest Target to your location that is most likely to have a COVID test kit in stock:',
          width=72
      )
  )
  print('')

  url_friendly_address = quote(
      f"Target {avails[best_choice_pid]['address'].replace(',', ' ')}"
  )
  table_raw = [
      [
          'Product price',
          products[best_choice_pid]['price'],
      ], [
          'Product name',
          products[best_choice_pid]['title'],
      ], [
          'Store name',
          avails[best_choice_pid]['store_name'],
      ],
      [
          'Store distance',
          f"{avails[best_choice_pid]['distance']} miles away",
      ],
      [
          'Store address',
          '\n'.join(avails[best_choice_pid]['address'].split(',')),
      ],
      [
          'Google Maps link',
          textwrap.fill(
              f"https://www.google.com/maps/search/{url_friendly_address}",
              width=50,
          ),
      ]
  ]

  output = tabulate(table_raw, tablefmt="fancy_grid")
  print(output)
  with open('table.txt', 'w+', encoding='utf8') as ftable:
    ftable.write(output)
    ftable.flush()
  exit(0)


async def main(args: Namespace) -> None:
  proxy_fac = RandomProxyHandlerFactory.genRandomProxyHandlerFactory(
      args.use_proxy,
  )
  query_params = QueryParams.genFromArgs(args)
  urlcleanup()
  context = Context.getFromArgs(args)
  env = Environment(await query_params, await proxy_fac, context)

  async def genLocationIDs(opener: OpenerDirector) -> Any:
    with opener.open(TargetEndpoints.createStoresURI(env)) as res:
      return {s['location_id']: s
              for s in json.load(res)['preferred_stores']}

  async def genSearchResults(opener: OpenerDirector, location_ids: Any) -> Any:
    with opener.open(
        TargetEndpoints.createSearchURI(env, list(location_ids.keys()))
    ) as res:
      return {
          p['tcin']: p
          for p in json.load(res)['data']['search']['products']
      }

  store_opener_task = await env.genOpenerForRequest()
  store_result_task = await genLocationIDs(
      store_opener_task
  )  # Location IDs here

  search_opener_task = await env.genOpenerForRequest()
  search_result_task = await genSearchResults(
      search_opener_task, store_result_task
  )  # Product IDs here

  async def genAvailability(env: Environment, product_id: ProductIDType) -> Any:
    opener = await env.genOpenerForRequest()
    with opener.open(
        TargetEndpoints.createAvailabilityURI(env, product_id)
    ) as res:
      return {
          product_id: l
          for l in json.load(res)['data']['fulfillment_fiats']['locations']
          if l['in_store_only']['availability_status'].lower() == 'in_stock'
      }

  tasks = [
      asyncio.create_task(genAvailability(env, product_id))
      for product_id in search_result_task
  ]

  availability = {}
  for avail in await asyncio.gather(*tasks):
    if avail:
      availability = availability | avail
  printTable(dt.now(), store_result_task, search_result_task, availability)


if __name__ == '__main__':
  parser = ArgumentParser(
      'find_covid_tests.py',
      description=
      'Proof of Concept. Search Target for COVID test kits within a given radius of a zipcode.',
  )

  # Arguments that affect API query parameters
  api_parser = parser.add_argument_group(
      'API args',
      'Changes API query parameters',
  )
  api_parser.add_argument(
      '-z',
      '--zip',
      metavar='ZIPCODE',
      help='Zipcode to search around (default: use GeoIP)',
  )
  api_parser.add_argument(
      '-r',
      '--radius',
      default='25',
      type=int,
      help='Radius to search from the provided or derived zipcode (default: 25)',
  )

  # Arguments that affect query strategy
  strat_parser = parser.add_argument_group(
      'Query strategy',
      'Changes how this tool approaches querying',
  )
  strat_parser.add_argument(
      '--use-proxy',
      type=FileType('r', encoding='utf-8'),
      metavar='PROXY_YAML',
      help='Enables the use of proxies to make queries',
  )
  strat_parser.add_argument(
      '-m',
      '--mood',
      choices=(MoodEnum),
      default='calm',
      help=
      'Whether the program will make requests slowly or as fast as possible',
  )

  # Arguments that affect program behavior
  behavior_parser = parser.add_argument_group(
      'Behavior',
      'Changes program behavior',
  )
  behavior_parser.add_argument(
      '-f',
      '--format',
      default='txt',
      choices=('json', 'txt'),
      help='Output format',
  )
  behavior_parser.add_argument(
      '-o',
      '--output',
      type=FileType('w+', encoding='utf-8'),
  )

  loop = asyncio.new_event_loop()
  loop.set_default_executor(ThreadPoolExecutor(cpu_count()))
  loop.run_until_complete(main(parser.parse_args()))
